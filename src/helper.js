// The code here is fetched from https://github.com/magiwanders/riscv-sliderules-cheatsheets/blob/main/src/isa/helper-functions.mjs

const { instructions } = require("../isa/instructions.js");
//import { immediates } from "./immediates.mjs";

// Helper function to extract the register number from register name
function _extractRegisterNumber({ register = "x0" }) {
  return parseInt(register.slice(1));
}

// Helper to transform the form {mnemonic: .. , operands: {...} } into plain assembly string 'add x1 x2 x2'
function _assembleInstruction({ mnemonic = "mnemonic", operands = {} }) {
  const assemblyOrder = instructions[mnemonic].assembly;
  const operandValues = assemblyOrder.map((field) => operands[field]).join(" ");
  return `${mnemonic} ${operandValues}`;
}

// Helper to transform the plain assembly string 'add x1 x2 x2' into the form {mnemonic: .. , operands: {...} }
function _parseAssemblyInstruction({
  assemblyString = "mnemonic op1 op2 op3",
}) {
  const [mnemonic, ...operandValues] = assemblyString.split(" ");
  const assemblyOrder = instructions[mnemonic].assembly;
  const instructionType = instructions[mnemonic].type;
  var operands = {};
  if (instructionType == "I" || instructionType == "R") {
    assemblyOrder.forEach((field, index) => {
      operands[field] = operandValues[index];
    });
  } else if (instructionType == "S") {
    operands = {
      rs1: operandValues[0],
      rs2: operandValues[1].split("(")[1].replace(/[()]/g, ""),
      imm: operandValues[1].split("(")[0],
    };
  }
  return { mnemonic, operands, instructionType };
}

// Helper function to convert decimal value given to hex and binary strings
function _binaryHexString({ value = 0 }) {
  const binary = `0b${(value >>> 0).toString(2).padStart(32, "0")}`;
  const hex = `0x${(value >>> 0).toString(16).padStart(8, "0").toUpperCase()}`;
  return { binary: binary, hex: hex };
}

// Helper function to extract the bit values using given mask
function _extractValues({ binaryvalue = 0b0, binaryMask = 0b0 }) {
  let result = 0;
  let shiftCount = 0;

  for (let i = 0; i < 32; i++) {
    const maskBit = (binaryMask >> i) & 1;
    if (maskBit === 1) {
      const valueBit = (binaryvalue >> i) & 1;
      result |= valueBit << shiftCount;
      shiftCount++;
    }
  }
  return result;
}

// Helper function to calculate shift of the fields
function _calculateShift({ mask = 0b0 }) {
  const binaryString = mask.toString(2).padStart(32, "0");
  const shiftingValue = 31 - binaryString.lastIndexOf("1");
  return shiftingValue;
}


// Function to decode a given assembly value
function decodeInstruction({ value = 0b0 }) {
  let mnemonic = null;
  let operands = null;
  for (const instructionName in instructions) {
    const instruction = instructions[instructionName];
    let match = true;

    for (const fieldName in instruction.fields) {
      const field = instruction.fields[fieldName];
      const expectedValue = _extractValues({
        binaryvalue: value,
        binaryMask: field.mask,
      });
      if (field.hasOwnProperty("value")) {
        const fieldValue = field.value;
        if (expectedValue !== fieldValue) {
          match = false;
          break;
        }
      } else {
        const registerNumber = _extractValues({
          binaryvalue: value,
          binaryMask: field.mask,
        });
        const registerName = `x${registerNumber}`;
        operands = operands || {};
        operands[fieldName] = registerName;
      }
    }

    if (match) {
      mnemonic = instructionName;
      break;
    }
  }
  return _assembleInstruction({ mnemonic: mnemonic, operands: operands });
}


//////////////////////////////////////////////////////////////////////////////////////////
// // Example usage

// const encodedInstruction = encodeInstruction({
//   //assemblyString: "sw x11, 512(x13)",
//   assemblyString: "sb x6, -8(x4)",
// });
// console.log(encodedInstruction.binary);

// // Example usage
// const instructionValue = encodedInstruction.binary; // Need to use '0b' representation
// const decodedInstruction = decodeInstruction({ value: 0b11111111000001000010011010110011 });
// console.log(decodedInstruction);

module.exports = {
  decodeInstruction,
};