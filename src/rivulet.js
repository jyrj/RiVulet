#!/usr/bin/env node

const fs = require("fs");
const { Command } = require('commander');
const { decodeInstruction } = require("./helper.js");

const program = new Command();

program
    .name('node-rivulet')
    .description('CLI to disassembe RV32|64IGC Instructions (Machine to Assembly)')
    .version('1.0.0');

program
  .option('-i, --input <type>', 'Path to input file for disassembly')
  .option('-o, --output <type>', 'Path to output the disassembed file');
  
program.parse(process.argv);

const options = program.opts()

if (options.length < 3) {
  console.error('Please provide a file path as a command line argument.');
  process.exit(1);
}


const filePath = options.input;
const outputFile = options.output || "././test/rivulet.out";

fs.readFile(filePath, "utf8", (err, data) => {
  if (err) {
    console.error("Error reading the file:", err);
    return;
  }

  const regex = /RIVU\((0x[0-9A-Fa-f]+)\)/g;
  const hexValues = [...data.matchAll(regex)].map((match) => match[1]);

  fs.writeFileSync(outputFile, "");
  for (const hexValue of hexValues) {
    const decodedInstruction = decodeInstruction({ value: hexValue });
    const outputLine = Object.values(decodedInstruction).join("") + "\n";
    fs.appendFileSync(outputFile, outputLine, "utf8");
  }

  console.log("Disassembling complete. Output written to:", outputFile);
});
