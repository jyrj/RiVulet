# RiVulet

A Simple RISC-V Disassembler based on Web Sliderules. See test folder to see sample tests.

```test\RV32I-sample.log``` shows a sample struture of machine code to get disassembled using RiVulet.

To build the binary using pkg: (tested on node 18)

```pkg ./src/rivulet.js --output ./rivulet```
```chmod +x ./rivulet```

```./rivulet /path/to/file.log /path/to/output.out```

If no output path provided, it can be found as ```rivulet.out```

You can see a sample .out file in the test folder to learn how the output look like

## Usage

Using node:

```node ./src/rivulet.js -i /path/to/file.log -o /path/to/output.out```

`-i or --input= can be used to input the input file location`

`-o or --output= can be used to give the location for output file`

You can see all options using ```node ./src/rivulet.js --help```